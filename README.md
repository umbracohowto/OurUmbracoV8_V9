# OurUmbraco Version 8 to Version 9 RC beta2

This repo contains a simple Umbraco 8 solution that has content in Danish & English

After the Umbraco 9 upgrade, document type translations seems to stop working.
Reported: https://github.com/umbraco/Umbraco-CMS/issues/10927

Login details:

```
{
	"Umbraco backoffice user" : {
		"username": "test@test.com",
		"password": "Mgj]O}7o{p"
	}
}
```
